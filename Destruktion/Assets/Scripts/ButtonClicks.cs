using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonClicks : MonoBehaviour
{
    public int buttonClicks;
    public int evilScore;
    public int goodScore;

    public bool evilEnding;
    public bool goodEnding;

    void Update()
    {

    }


    public void GoodClick()
    {
        buttonClicks++;

        if (buttonClicks < 6)
        {
        goodScore++;
        GameEvents.current.ScoreUpwards();
        }
        
        if(goodScore == 5)
        {
            goodEnding = true;
        }

        if(buttonClicks == 6)
        {
            PositiveEnd();
        }
    }

    public void EvilClick()
    {
        buttonClicks++;

        if(buttonClicks < 6)
        {
        GameEvents.current.ScoreDownwards();
        evilScore++;
        }

        if(buttonClicks == 6)
        {
            NegativEnd();
        }

        if(evilScore == 5)
        {
            evilEnding = true;
        }
    }
    public void PositiveEnd()
    {
        if(goodEnding == true)
        {
            Debug.Log("True Good");
            SceneManager.LoadScene("True Relax");
        }
        if(evilEnding == true)
        {
            Debug.Log("Evil Good");
            SceneManager.LoadScene("False Work");
        }
        if(!goodEnding && !evilEnding && evilScore < goodScore)
        {
            Debug.Log("Medium Good Good");
            SceneManager.LoadScene("Relax Optimistisch");
        }
        if (!goodEnding && !evilEnding && evilScore > goodScore)
        {
            Debug.Log("Medium Evil Good");
            SceneManager.LoadScene("Work Optimistisch");
        }
    }
    public void NegativEnd()
    {
        if (goodEnding == true)
        {
            Debug.Log("Good Evil");
            SceneManager.LoadScene("False Relax");
        }
        if (evilEnding == true)
        {
            Debug.Log("True Evil");
            SceneManager.LoadScene("True Work");
        }
        if(!goodEnding && !evilEnding && evilScore < goodScore)
        {
            Debug.Log("Medium Good Evil");
            SceneManager.LoadScene("Relax Pessimistisch");
        }
        if (!goodEnding && !evilEnding && evilScore > goodScore)
        {
            Debug.Log("Medium Evil Evil");
            SceneManager.LoadScene("Work Pessimistisch");
        }
    }
}
