using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionsAndAnswers : MonoBehaviour
{
    public GameObject[] questions;
    public GameObject[] goodAnswers;
    public GameObject[] evilAnswers;

    public int questionNumber;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.ScoreUp += NextQuestion;
        GameEvents.current.ScoreDown += NextQuestion;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < questions.Length; i++)
        {
            questions[i].SetActive(i == questionNumber);
        }

        for (int i = 0; i < goodAnswers.Length; i++)
        {
            goodAnswers[i].SetActive(i == questionNumber);
        }

        for (int i = 0; i < evilAnswers.Length; i++)
        {
            evilAnswers[i].SetActive(i == questionNumber);
        }
    }

    public void NextQuestion()
    {
        questionNumber++;
    }
}
