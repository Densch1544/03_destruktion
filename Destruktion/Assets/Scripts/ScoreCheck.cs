using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCheck : MonoBehaviour
{
    public GameObject[] smileys;
    public int smileyScore = 5;

    void Start()
    {
        GameEvents.current.ScoreUp += SmileyScoreUp;
        GameEvents.current.ScoreDown += SmileyScoreDown;
    }

    public void SmileyScoreUp()
    {
        smileyScore++;
    }

    public void SmileyScoreDown()
    {
        smileyScore--;
    }

    void Update()
    {
        for (int i = 0; i < smileys.Length; i++)
        {
            smileys[i].SetActive(i == smileyScore);
        }
    }
}
