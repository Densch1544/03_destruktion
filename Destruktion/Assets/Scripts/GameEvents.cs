using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    void Awake()
    {
        current = this;
    }

    public event Action ScoreUp;
    public void ScoreUpwards()
    {
        if (ScoreUp != null)
        {
            ScoreUp();
        }
    }

    public event Action ScoreDown;
    public void ScoreDownwards()
    {
        if (ScoreDown != null)
        {
            ScoreDown();
        }
    }

    public event Action PositiveDecision;
    public void PositiveEnding()
    {
        if (PositiveDecision != null)
        {
            PositiveDecision();
        }
    }
    public event Action NegativeDecision;
    public void NegativeEnding()
    {
        if (NegativeDecision != null)
        {
            NegativeDecision();
        }
    }
}
